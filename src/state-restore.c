/* 
 * state-utils
 * (c) 2005-2009 by Avuton Olrich <avuton@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <sys/types.h>
#include <pwd.h>
#include <ctype.h>
#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "libmpdclient.h"
#include "conn.h"
#include "state.h"

#define MPD_OUTPUT_STATE_DISABLED 0
#define LINELENGTH FILENAME_MAX+50	/* 50 to give adequate amount for command and args */
#define NEXTWORD(word) { word = strtok(NULL,":"); while(isspace(word[0])) { word++; } }
#define NEXTINT(ret,word) { NEXTWORD(word); ret = atoi(word); }

static struct Config {
	short int amend;
	short int noutput;
	short int nplaylist;
	unsigned int wait;
	char *extopts;
} config = {
0, 0, 0, 0, NULL};

static void readandexit(char *filename, char *dirname)
{
	errno = 0;

	access(dirname, F_OK);
	check_perror(errno, "Cannot read state file directory");

	access(dirname, X_OK);
	check_perror(errno, "Cannot read state file directory");

	access(dirname, R_OK);
	check_perror(errno, "Cannot read state file directory");

	access(filename, F_OK);
	check_perror(errno, "Cannot read state file");
}

static void restore_playlist(mpd_Connection * conn,
			     FILE * fp,
			     char *word, char buffer[LINELENGTH], int songnum)
{
	char *string;
	/* int number; */
	int curnum = 0;

	while (fgets(buffer, LINELENGTH, fp) &&
	       strcmp(word, "playlist_end\n") != 0 && strchr(buffer, ':')) {
		if (config.nplaylist == 1 && songnum != curnum) {
			curnum++;
			continue;
		}
		/*
		 * strtok_r would be called here, but this should
		 * be a bit more efficent since we discard the number.
		 */
		string = strchr(buffer, ':');
		*string = '\0';
		string++;
		string[strlen(string) - 1] = '\0';
		/* number = atoi(buffer); */
		mpd_sendAddCommand(conn, string);
		curnum++;
	}
}

static void restore_outputs(mpd_Connection * conn,
			    FILE * fp, char *word, char buffer[LINELENGTH])
{
	int number, namlen, stat;
	char *status, *name;

	while (fgets(buffer, LINELENGTH, fp) &&
	       strcmp(word, "outputs_end\n") != 0 && strchr(buffer, ':')) {
		number = atoi(buffer);

		status = strchr(buffer, ':');
		*status = '\0';
		status++;
		stat = atoi(status);

		name = strchr(status, ':');
		*name = '\0';
		name++;

		namlen = (strlen(name) - 1);
		if (name[namlen] == '\n') {
			name[namlen] = '\0';
		}

		if (stat == MPD_OUTPUT_STATE_DISABLED) {
			mpd_sendDisableOutputCommand(conn, number);
		} else {
			mpd_sendEnableOutputCommand(conn, number);
		}
	}
}

static void handle_state_contents(mpd_Connection * conn, FILE * fp)
{
	/* conv is a temporary converting variable */
	int conv = 0;

	/*
	 * We need these stored, incase the state
	 * is play or pause
	 */
	int state = 0;
	int seconds = MPD_PLAY_AT_BEGINNING;
	int songnum = 0;
	int volume = -1;

	char buffer[LINELENGTH];
	char *word;

	while (fgets(buffer, LINELENGTH, fp) && buffer != NULL) {
		word = strtok(buffer, ":");

		/* Special cases first */
		/* notice the strcmp ! ==0 */
		if (config.amend == 1 && strcmp(word, "playlist_begin\n")) {
			;	/* Do nothing */
		} else if (config.noutput == 1 &&
			   strcmp(word, "outputs_begin\n") == 0) {
			;	/* Do nothing */
		} else if (strcmp(word, "state") == 0) {
			NEXTWORD(word);
			if (strncmp(word, "play", strlen("play")) == 0) {
				state = MPD_STATUS_STATE_PLAY;
			} else if (strncmp(word, "pause", strlen("pause")) == 0) {
				state = MPD_STATUS_STATE_PAUSE;
			} else if (strncmp(word, "stop", strlen("stop")) == 0) {
				state = MPD_STATUS_STATE_STOP;
			} else {
				puts("Invalid state\n");
				exit(2);
			}
		} else if (strcmp(word, "current") == 0) {
			NEXTINT(songnum, word);
		} else if (strcmp(word, "volume") == 0) {
			/* Setvol to 0, try to reduce the seeking sounds */
			mpd_sendSetvolCommand(conn, volume);

			NEXTINT(volume, word);
		} else if (strcmp(word, "time") == 0) {
			NEXTINT(seconds, word);
		} else if (strcmp(word, "random") == 0) {
			NEXTINT(conv, word);
			mpd_sendRandomCommand(conn, conv);
		} else if (strcmp(word, "repeat") == 0) {
			NEXTINT(conv, word);
			mpd_sendRepeatCommand(conn, conv);
		} else if (strcmp(word, "single") == 0) {
			NEXTINT(conv, word);
			mpd_sendSingleCommand(conn, conv);
		} else if (strcmp(word, "consume") == 0) {
			NEXTINT(conv, word);
			mpd_sendConsumeCommand(conn, conv);
		} else if (strcmp(word, "crossfade") == 0) {
			NEXTINT(conv, word);
			mpd_sendCrossfadeCommand(conn, conv);
		} else if (strcmp(word, "playlist_begin\n") == 0) {
			if (config.amend == 0) {
				mpd_sendClearCommand(conn);
			}
			restore_playlist(conn, fp, word, buffer, songnum);
		} else if (strcmp(word, "outputs_begin\n") == 0) {
			restore_outputs(conn, fp, word, buffer);
		}
		word = strtok(NULL, ":");
	}

	if (config.nplaylist == 1) {
		mpd_sendSeekCommand(conn, 0, seconds);
	} else if (state != MPD_STATUS_STATE_STOP && config.amend == 0) {
		mpd_sendSeekCommand(conn, songnum, seconds);
		if (state == MPD_STATUS_STATE_PAUSE) {
			mpd_sendPauseCommand(conn, 1);
		}
	}

	if (volume != -1) {
		mpd_sendSetvolCommand(conn, volume);
	}
}

__attribute__ ((noreturn))
static void print_usage(int exitnum)
{
	printf("Usage: state-restore [OPTION] [STATE NAME]");
	printf("\nRestore a MPD state.");
	printf("\nExample: state-restore -a my_cool_state");
	printf("\n\n");
	printf
	    ("\n  -a, --amend\t\tAmend the playlist to the current MPD playlist, ignoring the actual state.");
	printf("\n  -o, --no-outputs\tOmit the outputs when restoring");
	printf("\n  -p, --no-playlist\tOnly restore the playing song");
	printf
	    ("\n  -w, --wait=TIMEOUT\tWait until the music player has stopped to restore the playlist. Check MPD every TIMEOUT seconds.");
	printf("\n\nReport bugs to Avuton Olrich <avuton@gmail.com>");
	printf("\nSee 'man 1 state-restore' for more information\n");
	exit(exitnum);
}

static FILE *get_file(void)
{
	char filename[FILENAME_MAX];
	char dirname[FILENAME_MAX];
	FILE *fp;

	sprintf(dirname, "%s/.mpd_states", get_home_dir());

	if (!(config.extopts)) {
		sprintf(filename, "%s/default", dirname);
	} else {
		sprintf(filename, "%s/%s", dirname, config.extopts);
	}

	fp = fopen(filename, "r");
	if (!fp) {
		readandexit(filename, dirname);
	}
	return fp;
}

static void parse_args(int argc, char **argv)
{
	int c;
	static struct option long_options[] = {
		{"amend", 0, 0, 'a'},
		{"help", 0, 0, 'h'},
		{"no-outputs", 0, 0, 'o'},
		{"no-playlist", 0, 0, 'p'},
		{"wait", 1, 0, 'w'},
		{0, 0, 0, 0}
	};

	while (1) {
		int option_index = 0;

		c = getopt_long(argc, argv, "ahopw:",
				long_options, &option_index);

		if (c == -1)
			break;

		switch (c) {

		case 'a':
			config.amend = 1;
			break;
		case 'o':
			config.noutput = 1;
			break;
		case 'p':
			config.nplaylist = 1;
			break;
		case 'w':
			if (optarg) {
				config.wait = atoi(optarg);
				if (config.wait) {
					if (config.wait < 1) {
						puts("Wait time must be longer than 0");
						print_usage(15);
					}
				} else {
					puts("Wait time must be a digit between 1 and 65535");
					print_usage(16);
				}
				/*
				 * This really shouldn't be possible, but let's not take the chance 
				 * this should be handled by getopts 
				 */
			} else {
				puts("Unknown Error!");
				print_usage(17);
			}
			break;
		case 'h':
		case '?':
			print_usage(0);
			break;
		default:
			print_usage(10);
			break;
		}
	}

	if (config.amend && config.nplaylist) {
		puts("Incompatible options");
		print_usage(12);
	}

	if (argc > optind) {
		if ((argc - optind) == 1) {
			config.extopts = argv[optind++];
		} else if ((argc - optind) > 1) {
			puts("Too many arguments, only restore one state at a time.");
			print_usage(13);
		}
	}
}

static void handle_wait(mpd_Connection * conn)
{
	mpd_Status *status;

	while (1) {
		if (!conn) {
			conn = setup_connection(NULL, NULL);
		}
		mpd_sendStatusCommand(conn);
		status = mpd_getStatus(conn);
		if (status->state == MPD_STATUS_STATE_STOP) {
			config.wait = 0;
		}
		printErrorAndExit(conn);
		mpd_finishCommand(conn);
		sleep(config.wait);
		if (config.wait == 0) {
			break;
		}
	}

	mpd_freeStatus(status);
}

int main(int argc, char **argv)
{
	FILE *fp;
	mpd_Connection *conn;

	parse_args(argc, argv);

	fp = get_file();

	conn = setup_connection(NULL, NULL);
	printErrorAndExit(conn);

	if (config.wait > 0) {
		handle_wait(conn);
	}

	mpd_sendCommandListBegin(conn);
	handle_state_contents(conn, fp);
	mpd_sendCommandListEnd(conn);
	printErrorAndExit(conn);
	mpd_finishCommand(conn);
	mpd_closeConnection(conn);

	return 0;
}
