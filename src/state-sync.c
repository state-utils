/* 
 * state-utils
 * (c) 2005-2009 by Avuton Olrich <avuton@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
/*
 * This program is far from perfect, but it doesn't get much better +/- 1 second lag between
 * MPD servers and the protocol like it is at the moment.
 */
#include "libmpdclient.h"
#include "conn.h"
#include "state.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>

#define MAX_HOSTS 8

static _Bool persistant = 1;
static _Bool force = 1;
static int host_num = 0;
/*
 * Storage for the connections
 */
static mpd_Connection *host_conn[MAX_HOSTS];
/*
 * Storage for the hosts/ports in case of the need for reconnection
 */
static char *globhost[MAX_HOSTS];
static char *globport[MAX_HOSTS];
/* */
__attribute__ ((noreturn))
static void print_usage(int exitnum)
{
	printf
	    ("Usage: state-sync [OPTION] [originhost]:[originport] [desthost1]:[destport1] ..."
	     "\nSync multiple MPD servers." "\n\n"
	     "\n  -f --force\t\tForce syncing when origin is in 'random' mode"
	     "\n  -h --help\t\tThis help page."
	     "\n  -p --persistant\tKeep all servers in sync with the origin."
	     "\n\nReport bugs to Avuton Olrich <avuton@gmail.com>"
	     "\nSee 'man 1 state-sync' for more information\n");
	exit(exitnum);
}
static void parse_args(int argc, char **argv)
{
	while (1) {
		int option_index = 0;
		int c;
		static struct option long_options[] = {
			{"help", 0, 0, 'h'},
			{"persistant", 0, 0, 'p'},
			{0, 0, 0, 0}
		};

		c = getopt_long(argc, argv, "hp", long_options, &option_index);

		if (c == -1)
			break;

		switch (c) {

		case 'f':
			force = 0;
			break;
		case 'p':
			persistant = 0;
			break;
		case 'h':
		case '?':
			print_usage(0);
			break;
		default:
			print_usage(1);
			break;
		}
	}
	/*
	 * Extract the host and ports here, store for later use
	 */
	while (argc > optind) {
		char *saveptr;
		globhost[host_num] = strtok_r(argv[optind], ":", &saveptr);
		globport[host_num] = strtok(saveptr, ":");
		optind++;
		/*
		 * Simply get a host count.
		 */
		if (globport[host_num] != NULL)
			host_num++;
	}
	/*
	 * Finally check for a minimum of 2 servers
	 */
	if (host_num < 2) {
		puts("Requires at least 2 servers to sync");
		print_usage(22);
	}
}

/*
 * int main()
 *
 * Expected action: This actually encompasses the whole program,
 * it's pretty hard and non-logical to break up this function
 * as everything's called once for a good reason.
 */
int main(int argc, char **argv)
{
	static int i;
	static mpd_Status *ostatus;
	/* Parse args will take care of all the arguments and connections */
	parse_args(argc, argv);
	/* */
	while (1) {
		for (i = 1; i < host_num; i++) {
			static mpd_InfoEntity *entity;
			static mpd_Song *song;
			/*
			 * Setup the inital connection
			 */
			if (host_conn[0] == NULL)
				host_conn[0] =
				    setup_connection(globhost[0], globport[0]);
			if (host_conn[i] == NULL)
				host_conn[i] =
				    setup_connection(globhost[i], globport[i]);
			/*
			 * Receive the baseline status from the origin MPD server
			 */
			mpd_sendStatusCommand(host_conn[0]);
			ostatus = mpd_getStatus(host_conn[0]);
			printErrorAndExit(host_conn[0]);
			/* */
			mpd_sendCommandListBegin(host_conn[i]);
			mpd_sendClearCommand(host_conn[i]);
			/*
			 * if user is running a 'ramdom' playlist warn that it cannot stay in sync
			 */
			if (ostatus->random == 0 && !force) {
				printf
				    ("Warning: Origin is currently in 'random' mode, cannot sync");
				continue;
			}
			/* 
			 * Setup to receive all the playlist information, then add to destination
			 */
			mpd_sendPlaylistInfoCommand(host_conn[0], -1);
			while ((entity = mpd_getNextInfoEntity(host_conn[0]))) {
				if (entity->type == MPD_INFO_ENTITY_TYPE_SONG) {
					song = entity->info.song;
					mpd_sendAddCommand(host_conn[i],
							   song->file);
				}
				mpd_freeInfoEntity(entity);
			}
			mpd_sendRandomCommand(host_conn[i],
					      ostatus->random ? 1 : 0);
			mpd_sendRepeatCommand(host_conn[i],
					      ostatus->repeat ? 1 : 0);
			if (ostatus->single)
				mpd_sendSingleCommand(host_conn[i],
							ostatus->single ? 1 : 0);
			if (ostatus->consume)
				mpd_sendConsumeCommand(host_conn[i],
							ostatus->consume ? 1 : 0);
			mpd_sendCrossfadeCommand(host_conn[i],
					      ostatus->crossfade ? 1 : 0);
			mpd_sendSetvolCommand(host_conn[i],
					      ostatus->volume ? 1 : 0);
			/*
			 * This will send the 'play' command as well
			 */
			if (ostatus->state == MPD_STATUS_STATE_PLAY)
				mpd_sendPauseCommand(host_conn[0], 1);
			if (ostatus->state != MPD_STATUS_STATE_STOP) {
				/*
				 * Start in sync, this causes a msec break in the song, if playing
				 */
				mpd_sendSeekCommand(host_conn[0], ostatus->song,
						    ostatus->elapsedTime);
				/*
				 * Send the rest of the commands to sync the state
				 */
				mpd_sendSeekCommand(host_conn[i], ostatus->song,
						    ostatus->elapsedTime);
			}
			/*
			 * We're gonna pause all if playing, we'll unpause at the same time to
			 * minimize lage. It's a bit of a hack that should work pretty well.
			 */
			if (ostatus->state == MPD_STATUS_STATE_PAUSE ||
			    ostatus->state == MPD_STATUS_STATE_PLAY)
				mpd_sendPauseCommand(host_conn[i], 1);

			mpd_sendCommandListEnd(host_conn[i]);
			mpd_finishCommand(host_conn[0]);
			printErrorAndExit(host_conn[i]);
			mpd_finishCommand(host_conn[i]);
		}
		/*
		 * Unpause if it was playing.
		 */
		if (ostatus->state == MPD_STATUS_STATE_PLAY) {
			/*
			 * Notice we start on 0 here.
			 */
			for (i = 0; i < host_num; i++) {
				mpd_sendPauseCommand(host_conn[i], 0);
			}
		}

		if (ostatus)
			mpd_freeStatus(ostatus);
		if (persistant)
			break;
		else
			sleep(5);
	}
	/*
	 * Close connections, cleanup
	 */
	mpd_closeConnection(host_conn[0]);
	for (i = 1; i < host_num; i++)
		mpd_closeConnection(host_conn[i]);

	return 0;
}
